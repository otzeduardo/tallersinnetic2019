﻿using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{
    #region Private Varaibles
    private float time;
    private readonly float timeMax = 2.5f;
    #endregion

    #region Public Variables
    public GameObject instaceEnemy;
    #endregion

    void Start()
    {
        Vector3 pos = new Vector3(Random.Range(10f, -10f), 25, Random.Range(10f, -10f));
        GameObject obj = Instantiate(instaceEnemy);
        obj.transform.position = pos;

    }

    void Update()
    {
        time += Time.deltaTime;
        if (time > timeMax)
        {
            time = 0;
            Vector3 pos = new Vector3(Random.Range(10f, -10f), 25, Random.Range(10f, -10f));
            GameObject obj = Instantiate(instaceEnemy);
            obj.transform.position = pos;
        }
    }
}