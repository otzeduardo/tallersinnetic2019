﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Molecules
{
    private Dictionary<int, Molecule> list;


    public Molecules()
    {
        Create();
    }

    private void Create()
    {
        list = new  Dictionary<int, Molecule>{};
        string[] data = File.ReadAllLines(Application.streamingAssetsPath + "/Data.dat");

        for(int i = 0; i < data.Length; i++)
        {
            string[] info = data[i].Split('-');
            string name = info[0];
            List<string> formula = new List<string>();

            for(int j = 0; j < info[1].Length; j++)
            {
                formula.Add(info[1].Substring(j, 1));
            }

            Molecule molecule;
            molecule.name = name;
            molecule.formula = formula;

            list.Add(i, molecule);
        }
    }

    public Dictionary<int, Molecule> Get()
    {
        return list;
    }
}

public struct Molecule
{
    public string name;
    public List<string> formula;
}