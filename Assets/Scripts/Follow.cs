﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    #region Public Variables
    public GameObject follow;
    public float distance = 2f;
    public float step = 10f;
    #endregion

    #region Private Variables
    private Rigidbody rigidbody;
    private Vector3 velocity;
    #endregion

    private void Awake()
    {
        GameManager gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        int index = gameManager.atoms.Count - 2;
        follow = gameManager.atoms[index];
        rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if(follow != null && Vector3.Distance(gameObject.transform.position, follow.transform.position) > distance)
        {
            Vector3 position = Vector3.SmoothDamp(transform.position, follow.transform.position,ref velocity, step * Time.deltaTime);
            transform.position = position;
        }

    }
}
