﻿using UnityEngine;
using TMPro;

public class SpawnAtoms : MonoBehaviour
{
    #region Private Varaibles
    private float time;
    private readonly float timeMax = 3;
    private int nAtoms;
    #endregion

    #region Public Variables
    public GameObject instaceAtom;
    #endregion

    void Start()
    {
        nAtoms = GameManager.formula.Count;
    }

    void Update()
    {
        time += Time.deltaTime;
        if(time > timeMax && nAtoms > 0)
        {
            time = 0;
            Vector3 pos = new Vector3(Random.Range(10f, -10f), 25, Random.Range(10f, -10f));
            GameObject obj = Instantiate(instaceAtom);
            obj.transform.position = pos;
            obj.transform.GetChild(0).GetComponent<TextMeshPro>().text = GameManager.formula[nAtoms - 1];
            nAtoms--;
        }
    }

    private void Reset()
    {

    }
}
