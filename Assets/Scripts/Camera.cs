﻿using UnityEngine;

public class Camera : MonoBehaviour
{

    #region Public Variables
    public float step;
    public float near;
    public float top;
    #endregion

    #region Private Variables
    private GameObject player;
    private GameObject camera;
    private Vector3 velocity = Vector3.zero;
    #endregion

    private void Awake()
    {
        player = GameObject.FindWithTag("Player");
        camera = GameObject.FindWithTag("MainCamera");
    }

    private void Update()
    {
        if(player != null)
        {
            Vector3 playerPos = new Vector3
            (player.transform.position.x, near, player.transform.position.z + top);

            Vector3 position = Vector3.SmoothDamp(
                camera.transform.position,
                playerPos,
                ref velocity,
                step);

            camera.transform.position = position;
        }
    }
}