﻿using UnityEngine;

public class Controller : MonoBehaviour
{
    #region Public Variables
    public float speed;
    public float step;
    public GameObject prefabPlayer;
    #endregion

    #region Private Variables
    private new Rigidbody rigidbody;
    private GameObject player;
    #endregion



    void Awake()
    {
        GameManager gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        gameManager.atoms.Add(gameObject);
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        Vector3 move = new Vector3(h, 0, v);

        rigidbody.transform.Translate(move * speed);

    }
}
