﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    #region Private Variables
    private int nMolecule;
    private Dictionary<int, Molecule> data;
    #endregion

    #region Public Variables
    [HideInInspector]
    public static List<string> formula;
    public List<GameObject> atoms = new List<GameObject>();
    #endregion


    void Awake()
    {
        Molecules molecules = new Molecules();
        data = molecules.Get();

        nMolecule = UnityEngine.Random.Range(0, data.Count);
        nMolecule = 0;

        TextMeshProUGUI molecule = GameObject.
            Find("Molecule").
            GetComponent<TextMeshProUGUI>();

        molecule.text = GetTextFormula();
        formula = data[nMolecule].formula;
    }

    private string GetTextFormula()
    {
        string formula = "";
        for(int i = 0; i < data[nMolecule].formula.Count; i++)
        {
            formula += data[nMolecule].formula[i] + " ";
        }
        return formula;
    }
}