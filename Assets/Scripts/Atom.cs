﻿using UnityEngine;
using TMPro;

public class Atom : MonoBehaviour
{
    #region Private Variables
    private TextMeshPro label;
    private AudioSource audioSource;
    private bool isFalling = true;
    #endregion

    #region Public Variables
    public bool activeCollision;
    public AudioClip collectSound;
    #endregion

    private void Awake()
    {
        name = transform.GetChild(0).GetComponent<TextMeshPro>().text;
        audioSource = GetComponent<AudioSource>();
    }

    public void SetName(string nameAtom)
    {
        label = transform.GetChild(0).GetComponent<TextMeshPro>();
        label.text = nameAtom;
        name = label.text;
    }

    private void Update()
    {
        if (isFalling)
        {
            transform.Translate(Vector3.down * Time.deltaTime * 20, Space.World);
            if (transform.position.y < 3)
                isFalling = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (activeCollision)
        {
            if (GameManager.formula.Contains(gameObject.name))
            {
                audioSource.clip = collectSound;
                audioSource.Play();

                GameManager.formula.Remove(collision.gameObject.name);
                GameManager gameManager = GameObject
                    .FindWithTag("GameManager")
                    .GetComponent<GameManager>();

                gameManager.atoms.Add(gameObject);
                gameObject.AddComponent<Follow>();
                gameObject.layer = 9;
                Destroy(GetComponent<Atom>());
            }
        }
    }

}
