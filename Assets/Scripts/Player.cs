﻿using UnityEngine;

public class Player : MonoBehaviour
{

    #region Public Variables
    public GameObject particlesDead;
    #endregion

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("DontTouch"))
        {
            GameObject particles =Instantiate(particlesDead);
            particles.transform.position = gameObject.transform.position;
            Destroy(gameObject);

        }
        else if (!GameManager.formula.Contains(collision.gameObject.name) && collision.gameObject.CompareTag("Atom"))
        {
            GameObject particles = Instantiate(particlesDead);
            particles.transform.position = gameObject.transform.position;
            Destroy(gameObject);
        }
    }
}
