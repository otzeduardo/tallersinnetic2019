﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    #region Private Variables
    private bool isFalling = true;
    #endregion

    private void Start()
    {

    }

    void Update()
    {
        if (isFalling)
        {
            transform.Translate(Vector3.down * Time.deltaTime * 45, Space.World);
            if (transform.position.y < 3)
            {
                StartCoroutine(DestroyInTime(4f));
                isFalling = false;
            }

        }
    }

    private IEnumerator DestroyInTime(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
